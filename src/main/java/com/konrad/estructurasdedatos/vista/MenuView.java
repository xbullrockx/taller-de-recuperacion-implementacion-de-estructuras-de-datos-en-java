/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.vista;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Bullrock
 */
public class MenuView extends JFrame{
    private JButton btnBusquedaAnchuraBFS;
    private JButton btnBusquedaProfundidadDFS;
    private JButton btnArbolesBinarios;
    private JButton btnAlgoritmoDijkstra;
    private JButton btnPilas;
    private JButton btnColas;
    private JButton btnArbolesB;
    private JButton btnBack;
    
    public MenuView() {
        setTitle("Main Menu");
        setSize(1000, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(4, 2));
        setVisible(true);

        btnBusquedaAnchuraBFS = new JButton("Búsqueda en anchura (BFS)");
        btnBusquedaProfundidadDFS = new JButton("Búsqueda en profundidad (DFS)");
        btnArbolesBinarios = new JButton("Árboles binarios");
        btnAlgoritmoDijkstra = new JButton("Algoritmo de Dijkstra");
        btnPilas = new JButton("Pilas");
        btnColas = new JButton("Colas ");
        btnArbolesB = new JButton("Árboles B+");

        add(btnBusquedaAnchuraBFS);
        add(btnBusquedaProfundidadDFS);
        add(btnArbolesBinarios);
        add(btnAlgoritmoDijkstra);
        add(btnPilas);
        add(btnColas);
        add(btnArbolesB);
    }

    public void addBusquedaAnchuraBFSListener(ActionListener listener) {
        btnBusquedaAnchuraBFS.addActionListener(listener);
    }

    public void addBusquedaProfundidadDFSListener(ActionListener listener) {
        btnBusquedaProfundidadDFS.addActionListener(listener);
    }

    public void addArbolesBinariosListener(ActionListener listener) {
        btnArbolesBinarios.addActionListener(listener);
    }
    
    public void addAlgoritmoDijkstraListener(ActionListener listener) {
        btnAlgoritmoDijkstra.addActionListener(listener);
    }

    public void addPilasListener(ActionListener listener) {
        btnPilas.addActionListener(listener);
    }

    public void addColasListener(ActionListener listener) {
        btnColas.addActionListener(listener);
    }
    
    public void addArbolesBListener(ActionListener listener) {
        btnArbolesB.addActionListener(listener);
    }

}
