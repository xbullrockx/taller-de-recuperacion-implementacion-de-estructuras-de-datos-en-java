/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.vista;

import com.konrad.estructurasdedatos.controlador.MenuController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Bullrock
 */
public class BackButton extends JButton {
    public BackButton(JFrame currentFrame) {
        super("Regresar");
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MenuView menu = new MenuView();
        MenuController controlador = new MenuController(menu);
                currentFrame.dispose();
            }
        });
    }
}