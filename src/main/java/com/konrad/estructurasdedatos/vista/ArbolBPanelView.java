/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.vista;
import com.konrad.estructurasdedatos.modelo.ArbolB;
import com.konrad.estructurasdedatos.modelo.ArbolBNode;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Bullrock
 */
public class ArbolBPanelView extends JPanel {
    private ArbolB bPlusTree;
    private int nodeWidth = 50;
    private int nodeHeight = 30;
    private int horizontalGap = 20;
    private int verticalGap = 50;

    public ArbolBPanelView(ArbolB bPlusTree) {
        this.bPlusTree = bPlusTree;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (bPlusTree != null) {
            drawNode(g, bPlusTree.root, getWidth() / 2, 30, horizontalGap * 2);
        }
    }

    private void drawNode(Graphics g, ArbolBNode node, int x, int y, int width) {
        int size = node.getKeys().size();
        int startX = x - (size * nodeWidth) / 2 - ((size - 1) * horizontalGap) / 2;
        for (int i = 0; i < size; i++) {
            int nodeX = startX + i * (nodeWidth + horizontalGap);
            g.drawRect(nodeX, y, nodeWidth, nodeHeight);
            g.drawString(String.valueOf(node.getKeys().get(i)), nodeX + 5, y + 20);
        }

        if (!node.isIsLeaf()) {
            int childY = y + verticalGap;
            for (int i = 0; i < node.getChildren().size(); i++) {
                int childX = startX + i * (nodeWidth + horizontalGap);
                int childWidth = node.getChildren().get(i).getKeys().size() * nodeWidth + (node.getChildren().get(i).getKeys().size() - 1) * horizontalGap;
                drawNode(g, node.getChildren().get(i), childX, childY, childWidth);
                if (i < size) {
                    int parentX = startX + (nodeWidth + horizontalGap) * i + nodeWidth / 2;
                    int parentY = y + nodeHeight;
                    g.drawLine(parentX, parentY, childX + childWidth / 2, childY);
                }
            }
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }
    
    
}
