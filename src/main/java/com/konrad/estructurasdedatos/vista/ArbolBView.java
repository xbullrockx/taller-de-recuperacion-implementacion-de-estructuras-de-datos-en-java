/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.vista;

import com.konrad.estructurasdedatos.controlador.ArbolBinarioController;
import com.konrad.estructurasdedatos.modelo.ArbolB;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Bullrock
 */
public class ArbolBView extends JFrame {

    private ArbolB bPlusTree;

    public ArbolBView() {
        bPlusTree = new ArbolB(3); // Grado mínimo de 3
        setTitle("Operaciones con Árbol B+");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        JButton insertButton = new JButton("Insertar");
        JButton searchButton = new JButton("Buscar");
        JButton displayButton = new JButton("Mostrar Árbol");
        JButton backButton = new BackButton(this);

        insertButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String value = JOptionPane.showInputDialog("Ingrese el valor a insertar:");
                bPlusTree.insert(Integer.parseInt(value));
                JOptionPane.showMessageDialog(null, "Valor insertado: " + value);
            }
        });

        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String value = JOptionPane.showInputDialog("Ingrese el valor a buscar:");
                boolean found = bPlusTree.search(Integer.parseInt(value));
                if (found) {
                    JOptionPane.showMessageDialog(null, "Valor encontrado: " + value);
                } else {
                    JOptionPane.showMessageDialog(null, "Valor no encontrado");
                }
            }
        });

        displayButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String treeContents = bPlusTree.display();
                JOptionPane.showMessageDialog(null, "Contenido del Árbol B+:\n" + treeContents);
              
                /*ArbolBPanelView visualizer = new ArbolBPanelView(bPlusTree);
                JFrame frame = new JFrame("Visualización de Árbol");
                frame.getContentPane().add(visualizer);
                frame.setSize(800, 600);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.setVisible(true);*/
            }
        });

        panel.add(insertButton);
        panel.add(searchButton);
        panel.add(displayButton);
        panel.add(backButton);
        add(panel);
    }
}
