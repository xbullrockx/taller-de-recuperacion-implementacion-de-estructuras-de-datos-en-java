/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.vista;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.ext.JGraphXAdapter;
import com.mxgraph.swing.mxGraphComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/**
 *
 * @author Bullrock
 */
public class GrafosView extends JFrame {
    private Graph<String, DefaultEdge> graph;

    public GrafosView() {
        graph = new SimpleGraph<>(DefaultEdge.class);
        setTitle("Operaciones con Grafos");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        JButton addNodeButton = new JButton("Añadir Nodo");
        JButton addEdgeButton = new JButton("Añadir Arista");
        JButton visualizeButton = new JButton("Visualizar");
        JButton backButton = new BackButton(this);
        

        addNodeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String node = JOptionPane.showInputDialog("Ingrese el nodo:");
                graph.addVertex(node);
                JOptionPane.showMessageDialog(null, "Nodo añadido: " + node);
            }
        });

        addEdgeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String node1 = JOptionPane.showInputDialog("Ingrese el primer nodo:");
                String node2 = JOptionPane.showInputDialog("Ingrese el segundo nodo:");
                graph.addEdge(node1, node2);
                JOptionPane.showMessageDialog(null, "Arista añadida entre: " + node1 + " y " + node2);
            }
        });

        visualizeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                visualizeGraph();
            }
        });

        panel.add(addNodeButton);
        panel.add(addEdgeButton);
        panel.add(visualizeButton);
        panel.add(backButton);
        add(panel);
    }

    private void visualizeGraph() {
        JGraphXAdapter<String, DefaultEdge> graphAdapter = new JGraphXAdapter<>(graph);
        mxGraphComponent component = new mxGraphComponent(graphAdapter);
        JFrame frame = new JFrame("Visualización de Grafo");
        frame.getContentPane().add(component);
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }
}

