/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.vista;

import com.konrad.estructurasdedatos.modelo.Pila;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Bullrock
 */
public class PilaView extends JFrame {
    private Pila stack;

    public PilaView() {
        stack = new Pila();
        setTitle("Operaciones con Pilas");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        JButton pushButton = new JButton("Insertar (Push)");
        JButton popButton = new JButton("Eliminar (Pop)");
        JButton searchButton = new JButton("Buscar");
        JButton displayButton = new JButton("Mostrar Pila");
        JButton backButton = new BackButton(this);

        pushButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String value = JOptionPane.showInputDialog("Ingrese el valor a insertar:");
                stack.push(Integer.parseInt(value));
                JOptionPane.showMessageDialog(null, "Valor insertado: " + value);
            }
        });

        popButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Integer value = stack.pop();
                if (value != null) {
                    JOptionPane.showMessageDialog(null, "Valor eliminado: " + value);
                } else {
                    JOptionPane.showMessageDialog(null, "La pila está vacía");
                }
            }
        });

        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String value = JOptionPane.showInputDialog("Ingrese el valor a buscar:");
                boolean found = stack.contains(Integer.parseInt(value));
                if (found) {
                    JOptionPane.showMessageDialog(null, "Valor encontrado en la pila: " + value);
                } else {
                    JOptionPane.showMessageDialog(null, "Valor no encontrado en la pila");
                }
            }
        });

        displayButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String stackContents = stack.displayStack();
                JOptionPane.showMessageDialog(null, "Contenido de la pila: " + stackContents);
            }
        });

        panel.add(pushButton);
        panel.add(popButton);
        panel.add(searchButton);
        panel.add(displayButton);
        panel.add(backButton);
        add(panel);
    }
}
