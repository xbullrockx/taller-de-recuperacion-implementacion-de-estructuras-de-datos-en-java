/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.vista;

import com.konrad.estructurasdedatos.modelo.Cola;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Bullrock
 */
public class ColaView extends JFrame {
    private Cola queue;

    public ColaView() {
        queue = new Cola();
        setTitle("Operaciones con Colas");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        JButton enqueueButton = new JButton("Insertar (Enqueue)");
        JButton dequeueButton = new JButton("Eliminar (Dequeue)");
        JButton searchButton = new JButton("Buscar");
        JButton displayButton = new JButton("Mostrar Cola");
        JButton backButton = new BackButton(this);

        enqueueButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String value = JOptionPane.showInputDialog("Ingrese el valor a insertar:");
                queue.enqueue(Integer.parseInt(value));
                JOptionPane.showMessageDialog(null, "Valor insertado: " + value);
            }
        });

        dequeueButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Integer value = queue.dequeue();
                if (value != null) {
                    JOptionPane.showMessageDialog(null, "Valor eliminado: " + value);
                } else {
                    JOptionPane.showMessageDialog(null, "La cola está vacía");
                }
            }
        });

        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String value = JOptionPane.showInputDialog("Ingrese el valor a buscar:");
                boolean found = queue.contains(Integer.parseInt(value));
                if (found) {
                    JOptionPane.showMessageDialog(null, "Valor encontrado en la cola: " + value);
                } else {
                    JOptionPane.showMessageDialog(null, "Valor no encontrado en la cola");
                }
            }
        });

        displayButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String queueContents = queue.displayQueue();
                JOptionPane.showMessageDialog(null, "Contenido de la cola: " + queueContents);
            }
        });

        panel.add(enqueueButton);
        panel.add(dequeueButton);
        panel.add(searchButton);
        panel.add(displayButton);
        panel.add(backButton);
        add(panel);
    }
}
