/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.vista;

import com.konrad.estructurasdedatos.controlador.ArbolBinarioController;
import com.konrad.estructurasdedatos.modelo.ArbolBinario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Bullrock
 */
public class ArbolBinarioView  extends JFrame {
    private ArbolBinario tree;

    public ArbolBinarioView() {
        tree = new ArbolBinario();
        setTitle("Operaciones con Árboles");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        JButton addNodeButton = new JButton("Añadir Nodo");
        JButton searchNodeButton = new JButton("Buscar Nodo");
        JButton visualizeButton = new JButton("Visualizar");
        JButton backButton = new BackButton(this);

        addNodeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String value = JOptionPane.showInputDialog("Ingrese el valor del nodo:");
                tree.insert(Integer.parseInt(value));
                JOptionPane.showMessageDialog(null, "Nodo añadido con valor: " + value);
            }
        });

        searchNodeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String value = JOptionPane.showInputDialog("Ingrese el valor a buscar:");
                boolean found = tree.search(Integer.parseInt(value));
                if (found) {
                    JOptionPane.showMessageDialog(null, "Nodo encontrado con valor: " + value);
                } else {
                    JOptionPane.showMessageDialog(null, "Nodo no encontrado");
                }
            }
        });

        visualizeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                visualizeTree();
            }
        });

        panel.add(addNodeButton);
        panel.add(searchNodeButton);
        panel.add(visualizeButton);
        panel.add(backButton);
        add(panel);
    }

    private void visualizeTree() {
        ArbolBinarioController visualizer = new ArbolBinarioController(tree);
        JFrame frame = new JFrame("Visualización de Árbol");
        frame.getContentPane().add(visualizer);
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }
}

