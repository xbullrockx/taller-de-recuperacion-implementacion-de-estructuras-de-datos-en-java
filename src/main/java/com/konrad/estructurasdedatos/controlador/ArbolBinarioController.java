/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.controlador;
import com.konrad.estructurasdedatos.modelo.ArbolBinario;
import javax.swing.*;
import java.awt.*;
/**
 *
 * @author Bullrock
 */
    /**
     * Clase para controlar la visualizacion de los arboles binarios
     */
public class ArbolBinarioController extends JPanel {
    private ArbolBinario tree;
    private int radius = 20;
    private int yIncrement = 50;

    //Constructor
    public ArbolBinarioController(ArbolBinario tree) {
        this.tree = tree;
        setPreferredSize(new Dimension(800, 600));
    }

    //Funcion para dibujar un componente
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (tree.root != null) {
            drawTree(g, tree.root, getWidth() / 2, 30, getWidth() / 4);
        }
    }

    //Funcion para dibujar arbol
    private void drawTree(Graphics g, ArbolBinario.Node root, int x, int y, int xOffset) {
        if (root != null) {
            
            g.fillOval(x - radius, y - radius, 2 * radius, 2 * radius);
            g.drawString(String.valueOf(root.getValue()), x - 6, y + 4);
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.white);
            g2.drawString(String.valueOf(root.getValue()), x - 6, y + 4);
            g.setColor(Color.black);

            if (root.getLeft() != null) {
                g.drawLine(x - radius, y, x - xOffset + radius, y + yIncrement);
                drawTree(g, root.getLeft(), x - xOffset, y + yIncrement, xOffset / 2);
            }

            if (root.getRight() != null) {
                g.drawLine(x + radius, y, x + xOffset - radius, y + yIncrement);
                drawTree(g, root.getRight(), x + xOffset, y + yIncrement, xOffset / 2);
            }
            
            
        }
    }
}

