package com.konrad.estructurasdedatos.controlador;

import com.konrad.estructurasdedatos.vista.ArbolBView;
import com.konrad.estructurasdedatos.vista.ArbolBinarioView;
import com.konrad.estructurasdedatos.vista.ColaView;
import com.konrad.estructurasdedatos.vista.GrafosView;
import com.konrad.estructurasdedatos.vista.MenuView;
import com.konrad.estructurasdedatos.vista.PilaView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class MenuController {

    private MenuView menuView;

    public MenuController(MenuView mainView) {
        this.menuView = mainView;
        this.menuView.addBusquedaAnchuraBFSListener(new BusquedaAnchuraBFSListener());
        this.menuView.addBusquedaProfundidadDFSListener(new BusquedaProfundidadDFSListener());
        this.menuView.addArbolesBinariosListener(new ArbolesBinariosListener());
        this.menuView.addAlgoritmoDijkstraListener(new AlgoritmoDijkstraListener());
        this.menuView.addPilasListener(new PilasListener());
        this.menuView.addColasListener(new ColasListener());
        this.menuView.addArbolesBListener(new ArbolesBListener());
    }

    class BusquedaAnchuraBFSListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            menuView.dispose();
            GrafosView grafosView = new GrafosView();
            grafosView.setVisible(true);
        }
    }

    class BusquedaProfundidadDFSListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(menuView, "No implementado aun");
        }
    }

    class ArbolesBinariosListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            menuView.dispose();
            ArbolBinarioView arbolBinarioView = new ArbolBinarioView();
            arbolBinarioView.setVisible(true);
        }
    }

    class AlgoritmoDijkstraListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(menuView, "No implementado aun");
        }
    }

    class PilasListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            menuView.dispose();
            PilaView pilasView = new PilaView();
            pilasView.setVisible(true);
        }
    }

    class ColasListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            menuView.dispose();
            ColaView colasView = new ColaView();
            colasView.setVisible(true);
        }
    }

    class ArbolesBListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            menuView.dispose();
            ArbolBView arbolBView = new ArbolBView();
            arbolBView.setVisible(true);
        }
    }
}
