/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.modelo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 *
 * @author Bullrock
 */
public class ArbolB {
    public ArbolBNode root;
    private int minDegree;

    //construccion del arbol b 
    public ArbolB(int minDegree) {
        this.minDegree = minDegree;
        this.root = new ArbolBNode(true);
    }

    //insertar valor
    public void insert(int key) {
        ArbolBNode root = this.root;
        if (root.keys.size() == 2 * minDegree - 1) {
            ArbolBNode newRoot = new ArbolBNode(false);
            newRoot.children.add(root);
            splitChild(newRoot, 0, root);
            this.root = newRoot;
        }
        insertNonFull(this.root, key);
    }

    private void insertNonFull(ArbolBNode node, int key) {
        if (node.isLeaf) {
            int pos = Collections.binarySearch(node.keys, key);
            if (pos < 0) pos = -pos - 1;
            node.keys.add(pos, key);
        } else {
            int pos = Collections.binarySearch(node.keys, key);
            if (pos < 0) pos = -pos - 1;
            ArbolBNode child = node.children.get(pos);
            if (child.keys.size() == 2 * minDegree - 1) {
                splitChild(node, pos, child);
                if (key > node.keys.get(pos)) {
                    pos++;
                }
            }
            insertNonFull(node.children.get(pos), key);
        }
    }

    private void splitChild(ArbolBNode parent, int index, ArbolBNode child) {
        ArbolBNode newNode = new ArbolBNode(child.isLeaf);
        int mid = minDegree - 1;

        parent.keys.add(index, child.keys.get(mid));
        parent.children.add(index + 1, newNode);

        newNode.keys.addAll(child.keys.subList(mid + 1, child.keys.size()));
        child.keys.subList(mid, child.keys.size()).clear();

        if (!child.isLeaf) {
            newNode.children.addAll(child.children.subList(minDegree, child.children.size()));
            child.children.subList(minDegree, child.children.size()).clear();
        }

        if (child.isLeaf) {
            newNode.next = child.next;
            child.next = newNode;
        }
    }

    public boolean search(int key) {
        return search(root, key);
    }

    private boolean search(ArbolBNode node, int key) {
        int pos = Collections.binarySearch(node.keys, key);
        if (pos >= 0) {
            return true;
        } else if (node.isLeaf) {
            return false;
        } else {
            pos = -pos - 1;
            return search(node.children.get(pos), key);
        }
    }

    public String display() {
        StringBuilder sb = new StringBuilder();
        display(root, sb, 0);
        return sb.toString();
    }

    private void display(ArbolBNode node, StringBuilder sb, int level) {
        sb.append("Nivel ").append(level).append(": ").append(node.keys).append("\n");
        if (!node.isLeaf) {
            for (ArbolBNode child : node.children) {
                display(child, sb, level + 1);
            }
        }
    }

    public ArbolBNode getRoot() {
        return root;
    }
    
    
}