/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.modelo;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Bullrock
 */
public class Pila {
    private List<Integer> stack;

    public Pila() {
        stack = new ArrayList<>();
    }

    //Ingresar valores a la pila
    public void push(int value) {
        stack.add(value);
    }

    //Sacar valores de la pila
    public Integer pop() {
        //Elimina el ultimo valor de la lista cuando no esta vacia
        if (!stack.isEmpty()) {
            return stack.remove(stack.size() - 1);
        }
        return null;
    }

    public boolean contains(int value) {
        return stack.contains(value);
    }

    //mostrar pila
    public String displayStack() {
        return stack.toString();
    }
}
