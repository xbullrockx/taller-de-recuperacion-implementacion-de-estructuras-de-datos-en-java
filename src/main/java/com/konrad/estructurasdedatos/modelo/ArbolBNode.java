/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bullrock
 */
public class ArbolBNode {
    List<Integer> keys;
    List<ArbolBNode> children;
    ArbolBNode next;
    boolean isLeaf;

    public ArbolBNode(boolean isLeaf) {
        this.isLeaf = isLeaf;
        this.keys = new ArrayList<>();
        this.children = new ArrayList<>();
        this.next = null;
    }

    public List<Integer> getKeys() {
        return keys;
    }

    public List<ArbolBNode> getChildren() {
        return children;
    }

    public ArbolBNode getNext() {
        return next;
    }

    public boolean isIsLeaf() {
        return isLeaf;
    }
}