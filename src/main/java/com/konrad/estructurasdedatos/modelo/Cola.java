/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.konrad.estructurasdedatos.modelo;

import java.util.LinkedList;
import java.util.Queue;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Bullrock
 */
public class Cola {
    private List<Integer> queue;

    public Cola() {
        queue = new ArrayList<>();
    }

    //Agregar valor a la cola
    public void enqueue(int value) {
        queue.add(value);
    }

    //Eliminar valor de la cola 
    public Integer dequeue() {
        if (!queue.isEmpty()) {
            return queue.remove(0);
        }
        return null;
    }

    public boolean contains(int value) {
        return queue.contains(value);
    }

    //Mostrar cola
    public String displayQueue() {
        return queue.toString();
    }
}

/*
    private Queue<Integer> queue;

    public Cola() {
        queue = new LinkedList<>();
    }

    public void enqueue(int value) {
        queue.add(value);
    }

    public Integer dequeue() {
        return queue.poll();
    }

    public boolean contains(int value) {
        return queue.contains(value);
    }

    public String displayQueue() {
        return queue.toString();
    }
}*/
