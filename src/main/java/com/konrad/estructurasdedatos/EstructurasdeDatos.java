package com.konrad.estructurasdedatos;

import com.konrad.estructurasdedatos.controlador.MenuController;
import com.konrad.estructurasdedatos.vista.MenuView;

/**
 *
 * @author Bullrock
 */
public class EstructurasdeDatos {


    public static void main(String[] args) {
        MenuView menu = new MenuView();
        MenuController controlador = new MenuController(menu);
    }
}
